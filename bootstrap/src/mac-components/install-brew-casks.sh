if [ -f ~/dotfiles/bash/aliases/common/utils.aliases.bash ]; then
    echo "Source utils.aliases.bash"
    source ~/dotfiles/bash/aliases/common/utils.aliases.bash
fi

# -----------------------------------------------------------------------------
# Brew Casks
# ----------------------------------------------------------------------------
export HOMEBREW_CASK_OPTS=--appdir=/Applications

divline
e_header "Installing some common Native Apps..."

# brew_install "brew-cask"

if confirm "Install development related apps"; then
    e_header "Installing development related apps..."
    cask_install "iterm2"
    cask_install "visual-studio-code"
    cask_install "sublime-text"
    cask_install "kaleidoscope"
    cask_install "sequel-pro"
    cask_install "postman"
    #cask_install "transmit"
    cask_install "packetsender"
    #cask_install "arduino"
    cask_install "etcher"
    cask_install "platypus"

    cask_install "tableplus"
    cask_install "virtualbox"
fi

if confirm "Install Android development related apps"; then
    e_header "Installing Android development related apps..."
    #cask_install "android-studio"
    cask_install "android-platform-tools"
    #cask_install "android-file-transfer"
    #cask_install "android-ndk"
    #cask_install "android-sdk"
    #cask_install "vysor"
fi

if confirm "Install browser apps"; then
    e_header "Installing browsers..."
    cask_install "google-chrome"
    cask_install "firefox"
    cask_install "opera"
fi

if confirm "Install image editing apps"; then
    e_header "Installing image editing apps..."
    cask_install "imagealpha"
    cask_install "imageoptim"
fi

if confirm "Install misc. helper apps"; then
    e_header "Installing misc. helper apps..."

    cask_install "namechanger"
    cask_install "appcleaner"
    cask_install "the-unarchiver"

    cask_install "transmission"
    cask_install "burn"
    cask_install "vlc"
    #cask_install "miro-video-converter"
    #cask_install "handbrake"
    #cask_install "ripit"
    #cask_install "fontexplorer-x-pro"
    #cask_install "google-earth"
    #cask_install "monolingual"
    #cask_install "audio-hijack"
    cask_install "adobe-acrobat-reader"
fi

if confirm "Install cloud apps"; then
    e_header "Installing cloud apps."
    cask_install "google-backup-and-sync"
    cask_install "dropbox"
fi

if confirm "Install communication apps"; then
    e_header "Installing communication apps."
    cask_install "slack"
    cask_install "skype"
    #cask_install "joinme"
fi

if confirm "Install office apps"; then
    e_header "Installing office apps."
    cask_install "microsoft-office"
    cask_install "omnigraffle"
fi

if confirm "Install Quicklook Plugins"; then
    e_header "Installing Quicklook Plugins"
    cask_install "webp-quicklook"
    cask_install "quicklook-csv"
    cask_install "quicklook-json"
    cask_install "qlcolorcode"
    cask_install "qlcolorcode"
    cask_install "qlimagesize"
    cask_install "qlmarkdown"
    cask_install "qlprettypatch"
    cask_install "qlstephen"
    # Restart QuickLook
    qlmanage -r
fi

if confirm "Install Alfred"; then
    e_header "Installing Alfred"
    cask_install "alfred"
fi
