#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Date / Time
# -----------------------------------------------------------------------------
alias date_now='date "+%Y-%m-%d"'

# ISO 8601
alias datetime='date "+%Y-%m-%d %H:%M:%S"'

# Date/Time safe for filenames (YYYY-MM-DD 00-00-00)
alias datetime_mac='date "+%Y-%m-%d %H-%M-%S"'

# Strict ISO 8601
alias datetime_iso='date "+%Y-%m-%dT%H:%M:%S%z"'

# UTC time (formerly known as GMT or Greenwich Mean Time)
alias datetime_utc='date -u "+%Y-%m-%d %H:%M:%SZ"'

# UTC time in ISO format
alias datetime_utc_iso='date -u "+%Y-%m-%dT%H:%M:%SZ"'

# Unix timestamp
alias datetime_unix="date +%s"

# -----------------------------------------------------------------------------
