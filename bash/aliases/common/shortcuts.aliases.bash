#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Bash Related Shortcuts
# -----------------------------------------------------------------------------
# Quick method to resource our .bash_profile
alias reload="source ~/.bash_profile"
alias reload_shell="exec $SHELL"

# Pull our latest dotfiles and symlink them
alias bs="cd ~/dotfiles/bin && ./symlink-dotfiles"

# History
alias h="history"
alias clearhistory="history -c"

# List aliases without the definition
aliases() {
    compgen -a | grep ${1-''}
}

# List functions without the definition
# Removes the 'declare -f' from before functions names
# Removes any lines starting with '_' as these are other app/system functions
functions() {
    declare -F | sed 's/declare -f //' | sed '/^_\(.*\)/d' | grep ${1-''}
}

# List aliases/functions - Forgot what we have available? This is awesome!
listaliases() {
    #aliases ${1} | sort | grep ${1-''}
    #functions ${1} | sort | grep ${1-''}

    # Combine the greps of both aliases/functions and sort
    local tmpfile=/tmp/aliases.$$.$RANDOM.txt
    aliases ${1} > $tmpfile
    functions ${1} >> $tmpfile
    cat < $tmpfile | sort | grep ${1-''}
    rm $tmpfile
}

# Echo all executable Paths
alias path='echo -e ${PATH//:/\\n}'

# Display bash options settings
alias show_options="shopt"

# Restore terminal settings when screwed up
alias fix_stty='stty sane'

# -----------------------------------------------------------------------------
