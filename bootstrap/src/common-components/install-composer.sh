# -----------------------------------------------------------------------------
# Install Composer
# -----------------------------------------------------------------------------
divline
e_header "Installing Composer..."

if confirm "Install composer"; then
    if is_not_installed "composer"; then

        e_installing "composer"

        EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

        if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
        then
            #>&2 echo 'ERROR: Invalid installer signature'
            rm composer-setup.php
            #exit 1
            e_error "Invalid installer signature"
        fi

        php composer-setup.php --quiet
        #RESULT=$?
        rm composer-setup.php
        #exit $RESULT

        # Places composer in the global environment
        execute "sudo mv composer.phar /usr/local/bin/composer" "Moved composer to /usr/local/bin/composer"

    fi
fi
