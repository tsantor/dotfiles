# -----------------------------------------------------------------------------
# Complete...tell the user what to do next
# -----------------------------------------------------------------------------
divline
e_success "DONE!"

e_header "To complete your setup, follow the guide(s):"

if [ "${OS}" = "OSX" ]; then

    e_command "Coming soon"

elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

    #e_command "https://bitbucket.org/tsantor/cheat-sheets/src/master/Server%20Setup/Ubuntu/ubuntu-php-deploy.md"
    e_command "Coming soon"

elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

    e_command "Coming soon"

fi

e_warning 'Remember to configure Apache and set up your virtual host.'
e_warning 'Remember to configure MySQL if you decide NOT to use a Cloud Database Instance.'

e_warning 'For full bootstrap details see the bootstrap.log'
