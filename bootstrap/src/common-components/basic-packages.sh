# -----------------------------------------------------------------------------
# Install some basic stuff
# -----------------------------------------------------------------------------
divline
e_header "Installing some basic stuff we need..."

if [ "${OS}" = "OSX" ]; then

    brew_tap "caskroom/cask"
    # brew_tap "caskroom/versions"

    # Ensures all tapped formula are symlinked into Library/Formula
    # and prunes dead formula from Library/Formula.
    #brew_tap_repair

    package_install "gnu-sed --with-default-names"
    package_install "grep --with-default-names"
    package_install "findutils --with-default-names"
    package_install "gnu-which --with-default-names"
    package_install "htop-osx"
    package_install "imagemagick"
    package_install "libiconv"
    package_install "ssh-copy-id"
    package_install "watch"
    package_install "mas"

elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

    package_install "htop"
    package_install "imagemagick"
    package_install "openssh-server"
    #package_install "watch"
    package_install "zip"
    package_install "build-essential"

elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

    package_install "htop"
    package_install "ImageMagick"
    package_install "lsof"
    package_install "mlocate"
    package_install "openssh-server"
    package_install "zip"

fi

# Common packages to all (eg - same name)
#package_install "shutil" # allows us to get OS name/version
package_install "bash"
package_install "cmake"
package_install "colordiff"
package_install "coreutils"
package_install "curl"
package_install "git"
package_install "grep"
package_install "nano"
package_install "nmap"
package_install "openssl"
package_install "rsync"
package_install "tree"
package_install "wget"
package_install "whois"
