# -----------------------------------------------------------------------------
# Install Postfix
# -----------------------------------------------------------------------------
divline
e_header "Installing Postfix..."

if confirm "Install Postfix"; then

    if [ "${OS}" = "OSX" ]; then

        e_todo

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "postfix"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "postfix"

    fi

    # Restart Postfix
    execute "postfix_restart" "Restarting Postfix..."

    # Start on boot
    execute "postfix_start_on_boot" "Start Postfix on boot"

fi
