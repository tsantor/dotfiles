# -----------------------------------------------------------------------------
# Install Ruby
# -----------------------------------------------------------------------------
divline
e_header "Installing Ruby..."

# Install ruby
package_install "ruby"

# Update gems
if confirm "Update Ruby Gems"; then

    e_header "Updating Ruby Gems..."
    sudo gem update --system

fi

# Install gems
divline
e_header "Installing Ruby Gems..."
gem_install "lunchy"
