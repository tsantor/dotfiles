#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Homebrew
# -----------------------------------------------------------------------------

BREWS_TEXT=~/Google\ Drive/Personal/brews.txt

brew_freeze() {
    brew list > "$BREWS_TEXT"
}

brew_sync() {
    if file_exists "$BREWS_TEXT"; then
        while read p; do
            # echo $p
            brew_install "$p"
        done < "$BREWS_TEXT"
    fi
}

alias brew_bundle="brew bundle dump --force --file=~/Google\ Drive/Personal/Brewfile"
alias brew_bundle_install="brew bundle install --file=~/Google\ Drive/Personal/Brewfile"


alias ibrew='arch -x86_64 /usr/local/bin/brew'
alias brew86="arch -x86_64 /usr/local/bin/brew"
alias pyenv86="arch -x86_64 pyenv"
alias pyenv="arch -x86_64 pyenv"
