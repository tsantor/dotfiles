#!/usr/bin/env bash

# Enable aliases to be sudo’ed
alias sudo='sudo '

# -----------------------------------------------------------------------------
# System
# -----------------------------------------------------------------------------
# Reboot, shutdown
alias reboot="sudo reboot"
alias shutdown="sudo shutdown -h now"

# -----------------------------------------------------------------------------
# Updates
# -----------------------------------------------------------------------------
# Update Composer
alias update_composer="composer self-update"

# -----------------------------------------------------------------------------
# Processes
# -----------------------------------------------------------------------------
# Top 10 processes being used
alias process_top10="ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10"

# Check if a process is running
# Example: findprocess httpd
process_find() {
    #ps -fA | grep ${1}
    ps aux | grep -v grep | grep -i -e VSZ -e ${1}
}

# Kill process
process_kill() {
    kill -9 ${1}
}

# -----------------------------------------------------------------------------
#
# -----------------------------------------------------------------------------
# Generate random password
alias generatepasswd="openssl rand -base64 6"

# Display public key
alias pubkey="cat ~/.ssh/id_rsa.pub"

# -----------------------------------------------------------------------------
# Misc
# -----------------------------------------------------------------------------
# Weather from my current location
alias weather="curl -s 'http://rss.accuweather.com/rss/liveweather_rss.asp?metric=0&locCode=en|us|sorrento-fl|32776' | sed -n '/Currently:/ s/.*: \(.*\): \([0-9]*\)\([CF]\).*/\2°\3, \1/p'"

# -----------------------------------------------------------------------------


# WTF
# http://kvz.io/blog/2012/10/03/quick-server-debugging-with-wtf/
alias wtf="tail -f /var/log/{dmesg,messages,*{,/*}{log,err}}"

alias python="/opt/homebrew/bin/python3"

# http://www.infoworld.com/article/3200766/application-development/9-shell-tips-every-developer-should-know.html
