# -----------------------------------------------------------------------------
# Update Packages
# -----------------------------------------------------------------------------
divline
e_header "Updating Installed Packages..."

if confirm "Update Installed Packages"; then

    update_packages

fi
