#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Visual Studio Code
# -----------------------------------------------------------------------------

VSCODE_EXTENSIONS_TEXT=~/Google\ Drive/Personal/vscode-extensions.txt

vscode_freeze() {
    code --list-extensions > "$VSCODE_EXTENSIONS_TEXT"
}

vscode_sync() {
    if file_exists "$VSCODE_EXTENSIONS_TEXT"; then
        while read line; do
            # echo $line
            code --install-extension $line || true
        done < "$VSCODE_EXTENSIONS_TEXT"
    fi
}
