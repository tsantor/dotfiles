#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Search
# -----------------------------------------------------------------------------
# Search for a file using MacOS Spotlight's metadata
spotlight () {
    mdfind "kMDItemDisplayName == '$@'wc"
}
