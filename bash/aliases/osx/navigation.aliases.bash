#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Common directories
# -----------------------------------------------------------------------------
# Change working directory to the top-most Finder window location
cdf() {
    cd "$(osascript -e 'tell app "Finder" to POSIX path of (insertion location as alias)')"
}

alias dl="cd ~/Downloads"
alias dt="cd ~/Desktop"
