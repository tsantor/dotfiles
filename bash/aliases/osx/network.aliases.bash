#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Network
# -----------------------------------------------------------------------------
# Turn WiFi on/off
alias wifi_on="networksetup -setairportpower airport on"
alias wifi_off="networksetup -setairportpower airport off"

# Flush directory service cache
alias flushdns="dscacheutil -flushcache; sudo killall -HUP mDNSResponder; echo '=> DNS cache flushed.'"

# -----------------------------------------------------------------------------
# IP Related
# -----------------------------------------------------------------------------
# Network address and local address
alias ip="dig +short myip.opendns.com @resolver1.opendns.com" #curl ifconfig.me
alias wiredip="ipconfig getifaddr en0"
alias wifiip="ipconfig getifaddr en1"
alias myip="ifconfig -u | grep 'inet ' | grep -v 127.0.0.1 | cut -d\  -f2 | head -1"

ipdetails() {
    echo "External IP: `ip`"
    echo "Wired IP: `wiredip`"
    echo "Wi-Fi IP: `wifiip`"
}

alias routerip="netstat -rn | grep default"

alias dnsservers="networksetup -getdnsservers Wi-Fi"

: <<'END'
alias myip='curl ip.appspot.com'                    # myip:         Public facing IP Address
alias netCons='lsof -i'                             # netCons:      Show all open TCP/IP sockets
alias flushDNS='dscacheutil -flushcache'            # flushDNS:     Flush out the DNS Cache
alias lsock='sudo /usr/sbin/lsof -i -P'             # lsock:        Display open sockets
alias lsockU='sudo /usr/sbin/lsof -nP | grep UDP'   # lsockU:       Display only open UDP sockets
alias lsockT='sudo /usr/sbin/lsof -nP | grep TCP'   # lsockT:       Display only open TCP sockets
alias ipInfo0='ipconfig getpacket en0'              # ipInfo0:      Get info on connections for en0
alias ipInfo1='ipconfig getpacket en1'              # ipInfo1:      Get info on connections for en1
alias openPorts='sudo lsof -i | grep LISTEN'        # openPorts:    All listening connections
alias showBlocked='sudo ipfw list'                  # showBlocked:  All ipfw rules inc/ blocked IPs
END

ii() {
    echo -e "\nYou are logged on ${MONOKAI_PINK}`hostname`${RESET}"
    echo -e "\nAdditionnal information:$NC "; uname -a
    echo -e "\n${MONOKAI_PINK}Users logged on:$NC${RESET}" ; w -h
    echo -e "\n${MONOKAI_PINK}Current date:$NC${RESET}"; date
    echo -e "\n${MONOKAI_PINK}Machine stats:$NC${RESET}"; uptime
    echo -e "\n${MONOKAI_PINK}Current network location:$NC${RESET}"; scselect
    echo -e "\n${MONOKAI_PINK}Public IP Address:$NC${RESET}"; ip
    echo -e "\n${MONOKAI_PINK}Internal IP Address:$NC${RESET}"; myip
    #echo -e "\n${RED}DNS Configuration:$NC " ; scutil --dns
    echo
}

alias listopenports="lsof -i -P | grep -i 'listen'"

getprocessid() {
    ps -A | grep -m1 $@ | awk '{print $1}'
}

whosonport() {
    sudo lsof -i :$@
}

kill_processes_on_port() {
    lsof -i tcp:$@
    lsof -t -i tcp:$@ | xargs kill
}
