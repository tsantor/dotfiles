# -----------------------------------------------------------------------------
# Install Apache
# -----------------------------------------------------------------------------
divline
e_header "Installing Apache..."

if confirm "Install Apache"; then

    set_server_name() {
        if ! cat /etc/apache2/apache2.conf | grep -q ^ServerName; then
            sudo sed -i "\$aServerName localhost" /etc/apache2/apache2.conf
            e_success "ServerName set to avoid Apache warning"
        fi
    }

    disable_ssl3() {
        if ! cat /etc/apache2/apache2.conf | grep -q ^SSLProtocol; then
            sudo sed -i "\$aSSLProtocol All -SSLv2 -SSLv3" /etc/apache2/apache2.conf
            e_success "Disabled SSL3 to avoid POODLE attacks"
        fi
    }

    install_apache_mods() {
        e_header "Install Apache mods..."
        # Install mods
        package_install "libapache2-mod-auth-mysql"
        package_install "libapache2-mod-wsgi"
        package_install "libapache2-mod-php5"

        # Enable mods
        execute "sudo a2enmod auth_mysql" "Mod auth_mysql"
        execute "sudo a2enmod php5"       "Mod php5"
        execute "sudo a2enmod headers"    "Mod headers"
        execute "sudo a2enmod expires"    "Mod expires"
        execute "sudo a2enmod ssl"        "Mod ssl"
        execute "sudo a2enmod wsgi"       "Mod wsgi"
        execute "sudo a2enmod rewrite"    "Mod rewrite"
    }

    open_http_and_https_ports() {
        e_header "Open ports 80 (http), 443 (https) and 22 (ssh)..."

        # Allow Incoming HTTP and HTTPS
        execute "sudo iptables -A INPUT -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 80 -m state --state ESTABLISHED -j ACCEPT"

        execute "sudo iptables -A INPUT -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 443 -m state --state ESTABLISHED -j ACCEPT"

        # Allow ALL Incoming SSH
        execute "sudo iptables -A INPUT -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT"

        # Save iptables
        e_header "Save the iptables..."
        execute "sudo iptables-save"

        e_header "Save the rules to be used on the next boot..."
        # Save the rules to be used on the next boot.
        sudo sed -i "\$apre-up iptables-restore < /etc/iptables.rules" /etc/network/interfaces
        sudo sed -i "\$apost-down iptables-save > /etc/iptables.rules" /etc/network/interfaces
    }

    # Install apache
    if is_package_not_installed "apache2"; then
        package_install "apache2"
        # package_install "apache2-mpm-prefork"
        # package_install "apache2-utils"

        # Start apache
        execute "apache_start" "Starting Apache..."

    fi

    set_server_name
    install_apache_mods
    #open_http_and_https_ports #TODO: this is locking us out of ssh?

    # Restart apache
    execute "apache_restart" "Restarting Apache..."

    # Start on boot
    execute "apache_start_on_boot" "Start Apache on boot"

fi
