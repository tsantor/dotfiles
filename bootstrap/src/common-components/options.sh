# -----------------------------------------------------------------------------
# Command Line Options
# -----------------------------------------------------------------------------
# Help text
if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    run_help
    exit 0
fi

# List additional software to install
#if [[ "$1" == "-l" || "$1" == "--list" ]]; then
#    run_list
#    exit
#fi

# Test for known flags
#for opt in $@
#do
#    case $opt in
#        --no-packages) no_packages=true ;;
#        --no-sync) no_sync=true ;;
#        -*|--*) e_warning "Warning: invalid option $opt" ;;
#    esac
#done
