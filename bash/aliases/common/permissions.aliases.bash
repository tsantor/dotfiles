#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Permissions
# -----------------------------------------------------------------------------
# Chmod all folders in the specified directory (does not affect files)
chmod_755dirs() {
    #find ${1} -type d -exec chmod 755 {} +
    # find ${1} -type d -exec chmod 0755 {} \;
    find ${1} -type d -print0 | xargs -0 chmod 755
}

chmod_750dirs() {
    #find ${1} -type d -exec chmod 755 {} +
    find ${1} -type d -print0 | xargs -0 chmod 750
}

# Chmod all files in the specified directory (does not affect folders)
chmod_644files() {
    #find ${1} -type f -exec chmod 644 {} +
    #find ${1} -type f -exec chmod 0644 {} \;
    find ${1} -type f -print0 | xargs -0 chmod 644
}

chmod_640files() {
    find ${1} -type f -print0 | xargs -0 chmod 640
}

# Make your directories and files access rights sane
chmod_sanitize() {
    chmod -R u=rwx,g=rx,o=x "$@"
}

# Show file/folder permissions
alias showperms="stat -c %a"

# -----------------------------------------------------------------------------
