# -----------------------------------------------------------------------------
# Help
# -----------------------------------------------------------------------------
function run_help() {

cat <<EOT
Tim's dotfiles - Tim Santor - http://xstudios.agency/

Usage: $(basename "$0") [options]

Options:
    -h, --help      Print this help text
    -l, --list      Print a list of additional software to install
    --no-packages   Suppress package updates
    --no-sync       Suppress pulling from the remote repository

Documentation can be found at https://bitbucket.org/tsantor/dotfiles/

Copyright (c) Tim Santor
Licensed under the MIT license.
EOT

}
