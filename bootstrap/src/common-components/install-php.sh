# -----------------------------------------------------------------------------
# Install PHP
# -----------------------------------------------------------------------------
divline
e_header "Installing PHP 5..."

if confirm "Install PHP 5"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "php56 --with-httpd24"

        divline
        # Install PHP modules
        e_header "Installing common PHP modules..."
        package_install "php56-imagick"
        package_install "php56-mcrypt"
        package_install "php56-memcached"
        package_install "php56-mongo"
        package_install "php56-redis"
        #package_install "php56-xdebug"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "php5"

        divline
        # Install PHP modules
        e_header "Installing common PHP modules..."
        package_install "php5-cli"
        package_install "php5-curl"
        package_install "php5-gd"
        package_install "php5-imagick"
        package_install "php5-mcrypt"
        package_install "php5-memcache"
        package_install "php5-mysql"
        package_install "php5-redis"
        package_install "php5-xdebug"

        sudo php5enmod mcrypt

    elif [ "${OS}" = "Fedora" ]; then

        package_install "php"

        divline
        # Install PHP modules
        e_header "Installing common PHP modules..."
        package_install "php-cli"
        package_install "php-common"
        package_install "php-gd"
        package_install "php-mbstring"
        package_install "php-mcrypt"
        package_install "php-mysql"
        package_install "php-pdo"
        package_install "php-pear"
        package_install "php-pecl-imagick"
        package_install "php-pecl-memcache"
        package_install "php-pecl-mongo"
        package_install "php-pecl-xdebug"
        package_install "php-redis"
        package_install "php-xml"

    elif [ "${OS}" = "CentOS" ]; then

        package_install "php"

        divline
        # Install PHP modules
        e_header "Installing common PHP modules..."
        package_install "php-cli"
        package_install "php-common"
        package_install "php-devel"
        package_install "php-fpm"
        package_install "php-gd"
        package_install "php-intl"
        package_install "php-markdown"
        package_install "php-mbstring"
        package_install "php-mcrypt"
        package_install "php-mysql"
        package_install "php-pdo"
        package_install "php-pecl-imagick"
        package_install "php-process"
        package_install "php-redis"
        package_install "php-xml"
        package_install "php-xmlrpc"
        package_install "php-zts"

    fi

    # Restart apache
    apache_restart

fi
