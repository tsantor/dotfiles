#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# PyPi
# -----------------------------------------------------------------------------
alias pypi_publish="python setup.py sdist bdist_wheel && twine upload dist/* -r pypi"
alias pypi_test="python setup.py sdist bdist_wheel && twine upload dist/* -r pypitest"
