#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Navigation
# -----------------------------------------------------------------------------
alias ~="cd ~"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls).
#-------------------------------------------------------------
# Always use color output for `ls`
# -l use a long listing format
# -p append / indicator to directories
# -h human-readable file sizes
# -v natural sort of (version) numbers within text
alias ls="ls -laphv -G"

# List all files
# -a do not ignore entries starting with .
alias la="ls -a"

# List only directories
alias ld='la | grep "^d"'

# List only files
alias lf='la | grep "^-"'

# List only symlinks
alias lsl='la | grep "^l"'

# List only hidden files
# -d list directories themselves, not their contents
alias lh="ls -d .*"

alias le="/bin/ls -al@"

# Change working directory and list the contents
cdl() {
    cd "$@" && la
}

numfiles() {
    echo $(ls -1 | wc -l)
}
