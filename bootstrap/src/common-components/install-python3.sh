# -----------------------------------------------------------------------------
# Install Python
# -----------------------------------------------------------------------------
divline
e_header "Installing Python 3..."

if confirm "Install Python 3"; then

    if [ "${OS}" = "OSX" ]; then

        if is_package_not_installed "python"; then

            package_install "python"
            brew link --overwrite python >> ${PWD}/bootstrap.log 2>&1

        fi

        pip install --upgrade pip

        e_header "Installing commonly required packages..."

        # Install packages
        package_install "libxml2"
        package_install "libxslt"

        # Install pip
        if is_not_installed "pip"; then
            sudo easy_install pip >> ${PWD}/bootstrap.log 2>&1
        fi

        # Install pip packages
        pip3_install "MySQL-python"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        e_header "Installing commonly required packages..."
        package_install "python-dev"

        # Needed to build cryptography
        package_install "libffi-dev"
        package_install "libssl-dev"

        # Needed to build mysql
        package_install "python-mysqldb"
        package_install "libmysqlclient-dev"

        # Install packages
        # package_install "libevent-dev"
        # package_install "libxml2-dev"
        #package_install "libxml2"
        # package_install "libxslt1-dev"
        #package_install "libxslt1.1"

        # Install pip
        package_install "python3-pip"
        package_install "python3-venv"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "python"

        # Needed to build mysql
        package_install "MySQL-python"

        # package_install "libxml2"
        # package_install "libxslt"

        # Install pip
        package_install "python3-pip"

    fi

    # Install pip packages
    e_header "Installing pip packages..."
    pip3_install "pip"
    pip3_install "setuptools"
    pip3_install "virtualenvwrapper"

fi
