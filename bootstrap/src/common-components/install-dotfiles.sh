# -----------------------------------------------------------------------------
# Install dotfiles
# -----------------------------------------------------------------------------
divline
e_header "Installing dotfiles..."

if confirm "Install dotfiles"; then

    if ! dir_exists "$HOME/dotfiles"; then

        git clone https://bitbucket.org/tsantor/dotfiles ~/dotfiles \
        && cd ~/dotfiles/bin \
        && ./symlink-dotfiles
        #&& source ~/.bash_profile

    else

        cd ~/dotfiles/bin && ./symlink-dotfiles

    fi

fi
