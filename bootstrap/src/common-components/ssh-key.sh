# -----------------------------------------------------------------------------
# SSH Key
# -----------------------------------------------------------------------------
divline
e_header "Checking for SSH key, generating one if it doesn't exist..."

if [[ ! -f ~/.ssh/id_rsa.pub ]]; then

    ssh-keygen -t rsa -C "tsantor@xstudios.com"

    e_header "Adding SSH Key to authentication agent"

    if [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ] || [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then
        # Start ssh agent before we attempt to add the key
        eval `ssh-agent -s`
    fi

    ssh-add ~/.ssh/id_rsa

else

    e_already_installed "SSH key"

fi
