# -----------------------------------------------------------------------------
# Install PHP
# -----------------------------------------------------------------------------
divline
e_header "Installing PHP 7..."

if confirm "Install PHP 7"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "php70 --with-httpd24"

        # Install PHP modules
        # e_header "Installing common PHP modules..."
        # package_install "php70-imagick"
        # package_install "php70-mcrypt"
        # package_install "php70-memcached"
        # package_install "php70-mongo"
        # package_install "php70-redis"
        #package_install "php70-xdebug"

    elif [ "${OS}" = "Debian" ]; then

        e_todo

    elif [ "${OS}" = "Ubuntu" ]; then

        package_install "php7.0"

        # Install PHP modules
        # e_header "Installing common PHP modules..."
        package_install "php7.0-cli"
        # package_install "php7.0-curl"
        # package_install "php7.0-gd"
        # package_install "php7.0-imagick"
        # package_install "php7.0-mcrypt"
        # package_install "php7.0-memcache"
        package_install "php7.0-mysql"
        # package_install "php7.0-redis"
        # package_install "php7.0-xdebug"
        package_install "php7.0-fpm"

        # sudo php5enmod mcrypt

    elif [ "${OS}" = "Fedora" ]; then

        package_install "php7.0"

        # Install PHP modules
        # e_header "Installing common PHP modules..."
        # package_install "php7.0-cli"
        # package_install "php7.0-common"
        # package_install "php7.0-gd"
        package_install "php7.0-mbstring"
        # package_install "php7.0-mcrypt"
        # package_install "php7.0-mysql"
        # package_install "php7.0-pdo"
        # package_install "php7.0-pear"
        # package_install "php7.0-pecl-imagick"
        # package_install "php7.0-pecl-memcache"
        # package_install "php7.0-pecl-mongo"
        # package_install "php7.0-pecl-xdebug"
        # package_install "php7.0-redis"
        # package_install "php7.0-xml"

    elif [ "${OS}" = "CentOS" ]; then

        package_install "php7.0"

        # Install PHP modules
        # e_header "Installing common PHP modules..."
        # package_install "php7.0-cli"
        # package_install "php7.0-common"
        # package_install "php7.0-devel"
        # package_install "php7.0-fpm"
        # package_install "php7.0-gd"
        # package_install "php7.0-intl"
        # package_install "php7.0-markdown"
        # package_install "php7.0-mbstring"
        # package_install "php7.0-mcrypt"
        # package_install "php7.0-mysql"
        # package_install "php7.0-pdo"
        # package_install "php7.0-pecl-imagick"
        # package_install "php7.0-process"
        # package_install "php7.0-redis"
        # package_install "php7.0-xml"
        # package_install "php7.0-xmlrpc"
        # package_install "php7.0-zts"

    fi

    # Restart apache
    # execute "apache_restart" "Restarting Apache..."
    e_warning "Remember to restart Apache and/or Nginx"

fi
