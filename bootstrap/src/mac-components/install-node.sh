# -----------------------------------------------------------------------------
# Install Node
# -----------------------------------------------------------------------------
divline
e_header "Installing Node..."

if confirm "Install Node"; then

    package_install "node"

    e_header "Installing essential NPM packages..."
    # Install Grunt (http://gruntjs.com/)
    npm_install "grunt-cli"

    # Install Gulp
    npm_install "gulp"

    # Install Yeoman (http://yeoman.io/)
    # npm_install "yo"
    # npm_install "generator-webapp"
    # npm_install "generator-angular"

    # Install Bower (http://bower.io/)
    npm_install "bower"

    # Install Express (http://expressjs.com/)
    npm_install "express"
    npm_install "express-generator"

    # Install Jade (http://jade-lang.com/)
    #npm_install jade

    # Install Stylus (http://learnboost.github.io/stylus/)
    #npm_install stylus

    # Install Forever (https://github.com/nodejitsu/forever)
    npm_install "forever"

    # Install NodeMailer (http://www.nodemailer.com/)
    #npm_install "nodemailer"

    # Install ImageOptim CLI (https://github.com/JamieMason/ImageOptim-CLI)
    npm_install "imageoptim-cli"

fi
