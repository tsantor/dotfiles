# -----------------------------------------------------------------------------
# Install Apache
# -----------------------------------------------------------------------------
divline
e_header "Installing Apache..."

# default Mac OS X Apache is in /etc/apache2/httpd.conf but we're
# using brew installed apache at /usr/local/etc/apache2/2.4/httpd.conf

set_server_admin() {
    if ! cat /usr/local/etc/apache2/2.4/httpd.conf | grep -q "ServerAdmin you@example.com"; then
        sudo sed -ir 's|ServerAdmin you@example.com|ServerAdmin tsantor@xstudiosinc.com|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null
    fi
}

set_server_name() {
    if ! cat /usr/local/etc/apache2/2.4/httpd.conf | grep -q "ServerName localhost"; then
        # sudo sed -i "\$aServerName localhost" /usr/local/etc/apache2/2.4/httpd.conf
        sudo sed -ir 's|#ServerName www.example.com:8080|ServerName localhost|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null
    fi
}

disable_ssl3() {
    if ! /usr/local/etc/apache2/2.4/httpd.conf | grep -q "SSLProtocol All -SSLv2 -SSLv3"; then
        sudo sed -i "\$aSSLProtocol All -SSLv2 -SSLv3" /usr/local/etc/apache2/2.4/httpd.conf
    fi
}

enable_vhosts_conf() {
    # Include httpd-vhosts.conf
    sudo sed -ir 's|#Include /usr/local/etc/apache2/2.4/extra/httpd-vhosts.conf|Include /private/etc/apache2/extra/httpd-vhosts.conf|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null
}

enable_modules() {
    # Enable vhost_alias_module
    sudo sed -ir 's|#LoadModule vhost_alias_module libexec/mod_vhost_alias.so|LoadModule vhost_alias_module libexec/mod_vhost_alias.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Enable php5_module
    #sudo sed -ir 's|#LoadModule php5_module libexec/libphp5.so|LoadModule php5_module /usr/local/Cellar/php55/5.5.30/libexec/apache2/libphp5.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # What about adding this?
    # Include /private/etc/apache2/other/*.conf

    # Enable rewrite_module
    sudo sed -ir 's|#LoadModule rewrite_module libexec/mod_rewrite.so|LoadModule rewrite_module libexec/mod_rewrite.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Enable mod_expires
    sudo sed -ir 's|#LoadModule expires_module libexec/mod_expires.so|LoadModule expires_module libexec/mod_expires.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Enable deflate_module
    sudo sed -ir 's|#LoadModule deflate_module libexec/deflate_module.so|LoadModule deflate_module libexec/deflate_module.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Enable wsgi_module
    # if ! cat /usr/local/etc/apache2/2.4/httpd.conf | grep -q "LoadModule wsgi_module /usr/local/Cellar/mod_wsgi/4.4.11/libexec/mod_wsgi.so"; then
    #     sudo sed -i "\$aLoadModule wsgi_module /usr/local/Cellar/mod_wsgi/4.4.11/libexec/mod_wsgi.so" /usr/local/etc/apache2/2.4/httpd.conf
    # fi

    # Set WSGI Socket Prefix to a user writable dir since we run under Hombebrew
    if ! cat /usr/local/etc/apache2/2.4/httpd.conf | grep -q "WSGISocketPrefix /Users/tsantor/run/wsgi"; then
        sudo sed -i "\$aWSGISocketPrefix /Users/tsantor/run/wsgi" /usr/local/etc/apache2/2.4/httpd.conf
    fi

    # Enable ssl_module
    # sudo sed -ir 's|#LoadModule ssl_module libexec/mod_ssl.so|LoadModule ssl_module /usr/libexec/mod_ssl.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null
}

enable_user_sites() {
    # Include httpd-userdir.conf
    sudo sed -ir 's|#Include /usr/local/etc/apache2/2.4/extra/httpd-userdir.conf|Include /private/etc/apache2/extra/httpd-userdir.conf|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Enable userdir_module
    sudo sed -ir 's|#LoadModule userdir_module libexec/mod_userdir.so|LoadModule userdir_module libexec/mod_userdir.so|g' /usr/local/etc/apache2/2.4/httpd.conf #&> /dev/null

    # Include user.conf
    sudo sed -ir 's|#Include /private/etc/apache2/users/\*.conf|Include /private/etc/apache2/users/\*.conf|g' /etc/apache2/extra/httpd-userdir.conf #&> /dev/null
}

if confirm 'Install httpd24'; then

    # Install apache
    if is_package_not_installed "httpd24"; then

        package_install "httpd24 --with-privileged-ports --with-brewed-ssl --with-http2"

    fi

    package_install "mod_wsgi  --with-homebrew-httpd24"

    execute "set_server_admin" "ServerAdmin set to tsantor@xstudiosinc.com"
    execute "set_server_name" "ServerName set to localhost to avoid Apache warning"
    execute "disable_ssl3" "Disabled SSL3 to avoid POODLE attacks"
    execute "enable_vhosts_conf" "Enabled vhost configuration"
    execute "enable_modules" "Enable required modules"
    execute "enable_user_sites" "Enable user Sites folder"

    # Start apache
    # e_header "Starting Apache..."
    # apache_restart

    # Start on boot
    # execute "apache_start_on_boot" "Start Apache on boot"

fi
