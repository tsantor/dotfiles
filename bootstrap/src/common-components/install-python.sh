# -----------------------------------------------------------------------------
# Install Python
# -----------------------------------------------------------------------------
divline
e_header "Installing Python..."

if confirm "Install Python"; then

    if [ "${OS}" = "OSX" ]; then

        if is_package_not_installed "python"; then

            package_install "python"
            brew link --overwrite python >> ${PWD}/bootstrap.log 2>&1

        fi

        pip install --upgrade pip

        e_header "Installing Pip and Virtualenvwrapper..."
        # Install packages
        package_install "libxml2"
        package_install "libxslt"

        # Install pip
        if is_not_installed "pip"; then
            sudo easy_install pip >> ${PWD}/bootstrap.log 2>&1
        fi

        # Install pip packages
        pip_install "MySQL-python"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "python-dev"

        e_header "Installing Pip and Virtualenvwrapper..."
        # Install packages
        package_install "libevent-dev"
        package_install "libxml2-dev"
        #package_install "libxml2"
        package_install "libxslt1-dev"
        #package_install "libxslt1.1"
        package_install "python-mysqldb"
        package_install "libmysqlclient-dev"

        # Install pip
        package_install "python-pip"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "python"

        e_header "Installing Pip and Virtualenvwrapper..."

        package_install "MySQL-python"
        package_install "libxml2"
        package_install "libxslt"

        # Install pip
        package_install "python-pip"

    fi

    # Install pip packages
    pip_install "pip"
    pip_install "setuptools"
    pip_install "virtualenvwrapper"

fi
