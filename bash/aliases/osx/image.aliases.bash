#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Image Manipulation
# -----------------------------------------------------------------------------
# Resize image proportionally
# Example: resizeimage 400 image.jpg
alias resizeimage="sips -Z "

# -----------------------------------------------------------------------------
# Image Optimization
# -----------------------------------------------------------------------------
# Run images through ImageAlpha and ImageOptim
alias imageoptim="imageOptim -a -q -d "

imageoptim_jpg() {
    find ${1} -name '*.jpg' | imageOptim -j -q
}

imageoptim_png() {
    find ${1} -name '*.png' | imageOptim -a -q
}

# -----------------------------------------------------------------------------
