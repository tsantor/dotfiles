#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# System
# -----------------------------------------------------------------------------
# List top cpu/memory hogging programs
alias memhog="top -l 1 -o rsize | head -20" #top -o mem
alias cpuhog="top -o cpu"

# -----------------------------------------------------------------------------
# Aliases to our "keg-only" brews (to not conflict with system versions)
# -----------------------------------------------------------------------------
#alias curl="/usr/local/Cellar/curl/7.38.0/bin/curl"

# -----------------------------------------------------------------------------
# Updates
# -----------------------------------------------------------------------------
# Get OS X Software Updates
alias update_sys="sudo softwareupdate -i -a"

# Update Homebrew
alias update_brew="brew update; brew upgrade; brew cleanup; brew doctor"

# Update Node
#alias update_node="sudo npm update npm -g; sudo npm update -g"

# Update NPM the "upstream-recommended way"
alias update_npm="npm install -g npm@latest"

# Update Ruby Gems
alias update_gems="sudo gem update"

# Update all of the above in one simple command
alias update_all='update_sys; update_brew; update_npm; update_gems; update_pip; update_composer'

# -----------------------------------------------------------------------------
# Download History
# -----------------------------------------------------------------------------
# Internet downloads
alias downloadhistory="sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'select LSQuarantineDataURLString from LSQuarantineEvent' | sort"
alias downloadhistory_clear="sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"

# App Store downloads
alias appstoredownloadhistory="find /Applications -path '*Contents/_MASReceipt/receipt' -maxdepth 4 -print |\sed 's#.app/Contents/_MASReceipt/receipt#.app#g; s#/Applications/##'"
