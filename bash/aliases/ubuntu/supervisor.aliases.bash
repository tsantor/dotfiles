#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# SUPERVISOR
# -----------------------------------------------------------------------------
alias supervisorctl_reread="sudo supervisorctl reread"
alias supervisorctl_update="sudo supervisorctl update"

alias supervisorctl_reset="supervisorctl_reread; supervisorctl_update; supervisorctl_restart all"

alias supervisorctl_add="sudo supervisorctl add "
alias supervisorctl_remove="sudo supervisorctl remove "
alias supervisorctl_status="sudo supervisorctl status "

alias supervisor_log="cat /tmp/supervisord.log"
alias supervisor_taillog="tail -f /tmp/supervisord.log"
alias supervisor_clearlog="echo '' > /tmp/supervisord.log"

# -----------------------------------------------------------------------------
# Shorthand
# -----------------------------------------------------------------------------
alias svctl_restart="supervisorctl_restart"
alias svctl_reread="supervisorctl_reread"
alias svctl_update="supervisorctl_update"
alias svctl_start="supervisorctl_start"
alias svctl_stop="supervisorctl_stop"
alias svctl_reset="supervisorctl_reset"

alias svctl_add="supervisorctl_add"
alias svctl_remove="supervisorctl_remove"

alias sv_log="supervisor_log"
alias sv_taillog="supervisor_taillog"
alias sv_clearlog="supervisor_clearlog"
