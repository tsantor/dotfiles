alias pip_freeze="python3 -m pip freeze > requirements.txt"

alias pip_offline="python3 -m pip freeze >> requirements.txt && pip download -r requirements.txt -d pypi-packages"
alias pip_offline_installreq="python3 -m pip install -r requirements.txt --find-links=pypi-packages"

alias pip_outdated="python3 -m pip list --outdated"
alias pip_upgradeall="python3 -m pip list --outdated | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 python3 -m pip install -U"
alias pip_removeall="python3 -m pip freeze --exclude-editable | grep -v '^-e git' | xargs python3 -m pip uninstall -y"
