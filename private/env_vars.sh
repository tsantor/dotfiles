# Stash your environment variables in ~/.env_vars. This means they'll stay out
# of your main dotfiles repository (which may be public, like this one), but
# you'll have access to them in your scripts.
if [ -f ~/.env_vars ]; then
    source ~/.env_vars
fi
