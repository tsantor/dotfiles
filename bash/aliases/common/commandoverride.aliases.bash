#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Command Overrides
# -----------------------------------------------------------------------------
alias ssh="ssh -o ServerAliveInterval=60 "

# Resume wget by default
alias wget='wget -c'

# Colorize the grep command output for ease of use
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Copy w/ progress
cp_p () {
    rsync -WavP --human-readable --progress --no-o --no-g ${1} ${2}
}

alias ping="ping -c 5"

# -----------------------------------------------------------------------------
# Safety Nets
# -----------------------------------------------------------------------------
# Do not delete /
#alias del='rm --preserve-root'

# Prevent accidentally clobbering files
#alias rm='rm -i'
#alias cp='cp -i'
#alias mv='mv -i'
#alias ln='ln -i'

# Parenting changing perms on /
#alias chown='chown --preserve-root'
#alias chmod='chmod --preserve-root'
#alias chgrp='chgrp --preserve-root'
