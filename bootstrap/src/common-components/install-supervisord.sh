# -----------------------------------------------------------------------------
# Install Supervisor
# -----------------------------------------------------------------------------
divline
e_header "Installing Supervisor..."

if confirm "Install Supervisor"; then

    if [ "${OS}" = "OSX" ]; then

        pip_install "supervisor" # On OSX installed through pip

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "supervisor"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "supervisor"

    fi

    # Start service
    execute "supervisor_start" "Starting supervisor..."

    # Start on boot
    execute "supervisor_start_on_boot" "Start Supervisor on boot"

fi
