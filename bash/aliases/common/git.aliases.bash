#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Functions
# -----------------------------------------------------------------------------
git_addorigin() {
    git remote add origin ${1}
}

git_tag() {
    git tag -a ${1} -m '${1}' ${2} && git push origin ${1}
}

git_deletetag() {
    if confirm 'Delete tag'; then
        git tag -d ${1} && git push origin :refs/tags/${1}
    fi
}

git_deletefile() {
    if confirm 'Delete file locally and remotely'; then
        git rm "$1" && git rm --cached "$1"
    fi
}

git_deletebranch() {
    if confirm 'Delete branch locally and remotely'; then
        git branch -D "$1" && git push origin --delete "$1"
    fi
}

git_check() {
    git remote update

    # magic to check if our copy is the same as the upstream copy
    UPSTREAM=${1:-'@{u}'}
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    # if our github copy needs a pull then we'll redo the distro
    if [[ $LOCAL == $REMOTE ]]; then
        echo "Up-to-date"
    elif [[ $LOCAL == $BASE ]]; then
        echo "Need to pull"
    elif [[ $REMOTE == $BASE ]]; then
        echo "Need to push"
    else
        echo "Diverged"
    fi
}

# -----------------------------------------------------------------------------
# Aliases
# -----------------------------------------------------------------------------
alias git_alias="git config --list | grep alias"

# -----------------------------------------------------------------------------
