# -----------------------------------------------------------------------------
# Install Apache
# -----------------------------------------------------------------------------
divline
e_header "Installing Apache..."

if confirm "Install Apache"; then

    set_server_name() {
        if ! cat /etc/httpd/conf/httpd.conf | grep -q ^ServerName; then
            sudo sed -i "\$aServerName localhost" /etc/httpd/conf/httpd.conf
            e_success "ServerName set to avoid Apache warning"
        fi
    }

    disable_ssl3() {
        if ! cat /etc/httpd/conf/httpd.conf | grep -q ^SSLProtocol; then
            sudo sed -i "\$aSSLProtocol All -SSLv2 -SSLv3" /etc/httpd/conf/httpd.conf
            e_success "Disabled SSL3 to avoid POODLE attacks"
        fi
    }

    install_apache_mods() {
        e_header "Install Apache mods..."
        # Install mods
        #package_install "mod_python"
        package_install "mod_wsgi"
        #package_install "mod_php"
        package_install "mod_ssl"
    }

    open_http_and_https_ports() {
        e_header "Open ports 80 (http), 443 (https) and 22 (ssh)..."

        # Allow Incoming HTTP and HTTPS
        execute "sudo iptables -A INPUT -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 80 -m state --state ESTABLISHED -j ACCEPT"

        execute "sudo iptables -A INPUT -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 443 -m state --state ESTABLISHED -j ACCEPT"

        # Allow ALL Incoming SSH
        execute "sudo iptables -A INPUT -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT"
        execute "sudo iptables -A OUTPUT -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT"

        # Save iptables
        e_header "Save the iptables..."
        execute "sudo service iptables save"

        #TODO: Save the rules to be used on the next boot.
    }

    # Install apache
    if is_package_not_installed "httpd"; then
        package_install "httpd"

        # Start apache
        execute "apache_start" "Starting Apache..."

    fi

    set_server_name
    disable_ssl3
    install_apache_mods
    #open_http_and_https_ports #TODO: this is locking us out of ssh?

    # Restart apache
    execute "apache_restart" "Restarting Apache..."

    # Start on boot
    execute "apache_start_on_boot" "Start Apache on boot"

fi
