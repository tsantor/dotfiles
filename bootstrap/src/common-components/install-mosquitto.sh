# -----------------------------------------------------------------------------
# Install Mosquitto
# -----------------------------------------------------------------------------
divline
e_header "Installing Mosquitto..."

if confirm "Install Mosquitto"; then

    if [ "${OS}" = "OSX" ]; then

        e_todo

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "mosquitto"
        package_install "mosquitto-clients"

    elif [ "${OS}" = "Fedora" ]; then

        e_todo

    elif [ "${OS}" = "CentOS" ]; then

        e_todo

    fi

    # Start service
    execute "mosquitto_start" "Starting Mosquitto..."

    # Start on boot
    # execute "mosquitto_start_on_boot" "Start Mosquitto on boot"

fi
