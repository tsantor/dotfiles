#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Shared Variables
# -----------------------------------------------------------------------------
# Colors
BOLD=$(tput bold)
RESET=$(tput sgr0)
MONOKAI_PINK=$(tput setaf 197)
MONOKAI_PURPLE=$(tput setaf 135)
MONOKAI_BLUE=$(tput setaf 81)
MONOKAI_GREEN=$(tput setaf 154)
MONOKAI_ORANGE=$(tput setaf 208)
MONOKAI_YELLOW=$(tput setaf 185)
MONOKAI_GREY=$(tput setaf 59)
MONOKAI_WHITE=$(tput setaf 231)

# Backgrounds
BG_RED='\e[41m'

# -----------------------------------------------------------------------------
# Get OS
# -----------------------------------------------------------------------------
get_os() {
    # Mac OSX
    if [[ `uname` == 'Darwin' ]]; then
        echo 'OSX'
        return 0
    fi

    # Note: If lsb_release not found run yum install redhat-lsb
    if [[ `uname` == 'Linux' ]]; then
        # If lsb_release then test that output
        if hash lsb_release &> /dev/null; then
            # Fedora
            if lsb_release -i | grep Fedora > /dev/null 2>&1; then
                echo 'Fedora'
                return 0
            fi

            # CentOS
            if lsb_release -i | grep CentOS > /dev/null 2>&1; then
                echo 'CentOS'
                return 0
            fi

            # Debian
            if lsb_release -i | grep Debian > /dev/null 2>&1; then
                echo 'Debian'
                return 0
            fi

            # Ubuntu
            if lsb_release -i | grep Ubuntu > /dev/null 2>&1; then
                echo 'Ubuntu'
                return 0
            fi

        # Try to cat the /etc/*release file
        else
            # Fedora
            if cat /etc/*release | grep Fedora > /dev/null 2>&1; then
                echo 'Fedora'
                return 0
            fi

            # CentOS
            if cat /etc/*release | grep CentOS > /dev/null 2>&1; then
                echo 'CentOS'
                return 0
            fi

            # Debian
            if cat /etc/*release | grep Debian > /dev/null 2>&1; then
                echo 'Debian'
                return 0
            fi

            # Ubuntu
            if cat /etc/*release | grep Ubuntu > /dev/null 2>&1; then
                echo 'Ubuntu'
                return 0
            fi

        fi
    fi

    echo 'unknown'
    return 0
}

# -----------------------------------------------------------------------------
# User Feedback
# -----------------------------------------------------------------------------
# Divider line
divline() {
    for i in {1..80};
        do printf "%s" "-";
    done;
    printf "\n"
}

# Script header
script_header() {
    #clear
    divline
    echo "${MONOKAI_WHITE}${1}${RESET} | Author: Tim Santor ${MONOKAI_GREY}<tsantor@xstudios.com>${RESET}"
    divline
}

# Header logging
e_header() {
    printf "${MONOKAI_GREEN}=> ${MONOKAI_WHITE}%s${RESET}\n" "$@"
}

# Success logging
e_success() {
    printf "${MONOKAI_GREEN}[✓] %s${RESET}\n" "$@"
}

# Error logging
e_error() {
    printf "${MONOKAI_PINK}[✗] %s${RESET}\n" "$@"
}

# Warning logging
e_warning() {
    printf "${MONOKAI_YELLOW}[!] %s${RESET}\n" "$@"
}

# Note
e_note() {
    printf "${MONOKAI_ORANGE}[→] %s${RESET}\n" "$@"
}

# Bash command example
e_command() {
    printf "    ${MONOKAI_GREY}%s${RESET}\n" "$@"
}

# Installing logging
e_installing() {
    printf "${MONOKAI_PURPLE}[≫] ${RESET}Installing ${MONOKAI_BLUE}%s${RESET}. Please wait...\n" "$@"
}

# Installed successfully logging
e_install_success() {
    printf "${MONOKAI_GREEN}[✓] ${MONOKAI_BLUE}%s${RESET} installed!\n" "$@"
}

# Install failed logging
e_install_error() {
    printf "${MONOKAI_PINK}[✗] ${RESET}Failed to install ${MONOKAI_PINK}%s${RESET}\n" "$@"
}

# Already installed logging
e_already_installed() {
    printf "${MONOKAI_GREEN}[✓] ${MONOKAI_BLUE}%s${RESET} already installed. ${MONOKAI_GREEN}Skipping...${RESET}\n" "$@"
}

# User declined logging
e_declined() {
    printf "${MONOKAI_YELLOW}[✗] ${MONOKAI_BLUE}%s${RESET} declined. ${MONOKAI_GREEN}Skipping...${RESET}\n" "$@"
}

# Show todo message
e_todo() {
    e_warning "TODO: not implemented"
}

# Show alert message
e_alert() {
    printf "${MONOKAI_WHITE}${BG_RED} ${1} ${RESET}\n"
}

# -----------------------------------------------------------------------------
# User Input
# -----------------------------------------------------------------------------
# Ask a question
e_prompt() {
    echo -n "${MONOKAI_YELLOW}[?] ${MONOKAI_BLUE}$1 ${RESET}"
}

# Ask a question
e_question() {
    echo -n "${MONOKAI_YELLOW}[?] ${MONOKAI_BLUE}$1? ${RESET}"
}

# Prompt to continue - Are you sure (y/n)?
confirm() {
    read -p "${MONOKAI_YELLOW}[?]${RESET} ${MONOKAI_BLUE}$1?${RESET} (y/n) "
    if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        return 0
    else
        e_declined "${1}"
        return 1
    fi
}

# Ask for confirmation before proceeding
seek_confirmation() {
    printf "\n"
    read -p "$1 Are you sure (y/n)? " -n 1
    printf "\n"
}

# Test whether the result of an 'ask' is a confirmation
is_confirmed() {
    if [[ "$REPLY" =~ ^[Yy]$ ]]; then
        return 0
    fi
    return 1
}

# -----------------------------------------------------------------------------
# Execute Command
# -----------------------------------------------------------------------------
# Test whether a command exists
# $1 - cmd to test
type_exists() {
    if [ $(type -P $1) ]; then
        return 0
    fi
    return 1
}

print_result() {
    [ $1 -eq 0 ] && e_success "$2" || e_error "$2"

    [ "$3" == "true" ] && [ $1 -ne 0 ] && exit
}

execute() {
    $1 &> /dev/null
    print_result $? "${2:-$1}"
}

# -----------------------------------------------------------------------------
# Error Handling
# -----------------------------------------------------------------------------
# A slicker error handling routine

# I put a variable in my scripts named PROGNAME which
# holds the name of the program being run.  You can get this
# value from the first item on the command line ($0).
# eg - PROGNAME=$(basename "$0")

error_exit() {
    e_error "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    exit 1
}

ensure_args() {
    if ! [ $# -gt 0 ]; then
        e_error "Your command contains no arguments"
        exit 1
    fi
}

# -----------------------------------------------------------------------------
# User Detection
# -----------------------------------------------------------------------------
check_root() {
    if [[ $EUID -ne 0 ]]; then
       e_error "This script must be run as root" 1>&2
       exit 1
    fi
}

# -----------------------------------------------------------------------------
# Package Installation
# -----------------------------------------------------------------------------
is_not_installed() {
    if ! hash ${1} &> /dev/null; then
        return 0
    else
        e_already_installed ${1}
        return 1
    fi
}

is_package_not_installed() {
    if [ "${OS}" = "OSX" ]; then
        if ! brew list | grep -q ^${1}$ &> /dev/null; then
            return 0
        else
            e_already_installed ${1}
            return 1
        fi

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then
        if ! dpkg --get-selections | grep ^${1} &> /dev/null; then
            return 0
        else
            e_already_installed ${1}
            return 1
        fi

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then
        if ! rpm -qa | grep -q ^${1} &> /dev/null; then
            return 0
        else
            e_already_installed ${1}
            return 1
        fi
    fi
}

install_package() {
    e_installing ${1}

    if [ "${OS}" = "OSX" ]; then
        if brew install ${1} >> ${PWD}/bootstrap.log 2>&1; then
            e_install_success ${1}
            return 0
        else
            e_install_error ${1}
            return 1
        fi

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then
        if sudo apt-get install -y ${1} >> ${PWD}/bootstrap.log 2>&1; then
            e_install_success ${1}
            return 0
        else
            e_install_error ${1}
            return 1
        fi

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then
       if sudo yum install -y ${1} >> ${PWD}/bootstrap.log 2>&1; then
            e_install_success ${1}
            return 0
        else
            e_install_error ${1}
            return 1
        fi
    fi
}

# Install package via system package manager
package_install() {
    if is_package_not_installed ${1}; then
        install_package ${1}
    fi
}

package_search() {
    if [ "${OS}" = "OSX" ]; then
        brew search ${1}
    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then
        apt-cache search ${1}
    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then
        yum list ${1}
    fi
}

# -----------------------------------------------------------------------------
# Pip Packages
# -----------------------------------------------------------------------------
is_pip_package_not_installed() {
    if ! pip list | grep ${1} &> /dev/null; then
        return 0
    else
        e_already_installed ${1}
        return 1
    fi
}

install_pip_package() {
    e_installing ${1}

    if pip install --upgrade ${1} >> ${PWD}/bootstrap.log 2>&1; then
        e_install_success ${1}
        return 0
    else
        e_install_error ${1}
        return 1
    fi
}

# Install package via pip
pip_install() {
    if is_pip_package_not_installed "$@"; then
        install_pip_package "$@"
    fi
}

# -----------------------------------------------------------------------------
# Pip3 Packages
# -----------------------------------------------------------------------------
is_pip3_package_not_installed() {
    if ! pip3 list | grep ${1} &> /dev/null; then
        return 0
    else
        e_already_installed ${1}
        return 1
    fi
}

install_pip3_package() {
    e_installing ${1}

    if sudo pip3 install --upgrade ${1} >> ${PWD}/bootstrap.log 2>&1; then
        e_install_success ${1}
        return 0
    else
        e_install_error ${1}
        return 1
    fi
}

# Install package via pip
pip3_install() {
    if is_pip3_package_not_installed "$@"; then
        install_pip3_package "$@"
    fi
}


# -----------------------------------------------------------------------------
# Ruby gems
# -----------------------------------------------------------------------------
is_ruby_gem_not_installed() {
    # We don't do a regex grep here as it causes issues
    # since gem list reports the version number too
    if ! gem list | grep -q ${1}; then
        return 0
    else
        e_already_installed ${1}
        return 1
    fi
}

install_gem() {
    e_installing ${1}

    if sudo gem install ${1} >> ${PWD}/bootstrap.log 2>&1; then
        e_install_success ${1}
        return 0
    else
        e_install_error ${1}
        return 1
    fi
}

gem_install() {
    if is_ruby_gem_not_installed "$@"; then
        install_gem "$@"
    fi
}

# -----------------------------------------------------------------------------
# NPM
# -----------------------------------------------------------------------------
is_npm_not_installed() {
    if ! npm list -g --depth=0 | grep -q ${1}@; then
        return 0
    else
        e_already_installed ${1}
        return 1
    fi
}

install_npm() {
    e_installing ${1}

    if npm install -g ${1} >> ${PWD}/bootstrap.log 2>&1; then
        e_install_success ${1}
        return 0
    else
        e_install_error ${1}
        return 1
    fi
}

npm_install() {
    if is_npm_not_installed "$@"; then
        install_npm "$@"
    fi
}

# -----------------------------------------------------------------------------
# Homebrew
# -----------------------------------------------------------------------------
brew_update() {
    e_header "Updating Homebrew..."
    # Use the latest version of Homebrew
    brew update
    [[ $? ]] && e_success "DONE!"
}

brew_upgrade() {
    e_header "Upgrading any existing Homebrew formulae..."
    # Upgrade any already-installed formulae
    brew upgrade
    [[ $? ]] && e_success "DONE!"
}

brew_doctor() {
    e_header "Running brew doctor..."
    # Run brew doctor to detect possible issues
    brew doctor
    [[ $? ]] && e_success "DONE!"
}

brew_cleanup() {
    e_header "Cleaning up outdated versions..."
    # Remove outdated versions from the Cellar
    brew cleanup
    [[ $? ]] && e_success "DONE!"
}

brew_install() {
    package_install ${1}
}

brew_tap() {
    if ! brew tap | grep -q "^${1}$"; then
        e_header "Brew tapping ${1}..."
        brew tap ${1} #2> /dev/null
    else
        e_already_installed ${1}
    fi
}

brew_tap_repair() {
    e_header "Repairing brew taps..."
    # Ensures all tapped formula are symlinked into Library/Formula
    # and prunes dead formula from Library/Formula.
    brew tap --repair
    [[ $? ]] && e_success "DONE!"
}

cask_install() {
    if ! brew cask list | grep -q "^${1}$"; then
        e_header "Brew cask installing ${1}..."
        brew cask install ${1} #2> /dev/null
    else
        e_already_installed ${1}
    fi
}

# -----------------------------------------------------------------------------
# File Location
# -----------------------------------------------------------------------------
file_exists() {
    if [ ! -f "$@" ]; then
        #e_error "File not found: $@"
        return 1
    else
        return 0
    fi
}

dir_exists() {
    if [ ! -d "$@" ]; then
        #e_error "Dir not found: $@"
        return 1
    else
        return 0
    fi
}

# php.ini
get_phpini_location() {
    php -i 2>&1 | grep "Loaded Configuration File" | cut -d '>' -f 2 | tr -d ' '
}

# -----------------------------------------------------------------------------

source_it() {
    if [ -f ${1} ]; then
        source ${1}
        e_success "Sourced: ${1}"
    fi
}

update_packages() {
    if [ "${OS}" = "OSX" ]; then

        # Update package list
        brew_update
        # Install newest packages
        brew_upgrade
        brew_doctor
        brew_cleanup

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        # Update package list
        sudo apt-get update
        # Install newest packages
        sudo apt-get upgrade -y

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        # Update package list
        sudo yum list updates
        # Install newest packages
        sudo yum update -y

    fi
}

# -----------------------------------------------------------------------------
# Log message to file
# -----------------------------------------------------------------------------
log() {
    ts=`date "+%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] $@" >> ~/bashscript.log
    echo "$@"
}

log_warning() {
    ts=`date "+%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] WARNING: $@" >> ~/bashscript.log
    echo "WARNING: $@"
}

log_error() {
    ts=`date "+%Y-%m-%d %H:%M:%S"`
    echo "[${ts}] ERROR: $@" >> ~/bashscript.log
    echo "ERROR: $@"
}

# -----------------------------------------------------------------------------
