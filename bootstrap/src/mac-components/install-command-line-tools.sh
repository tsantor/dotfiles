# -----------------------------------------------------------------------------
# Install Command Line Tools
# -----------------------------------------------------------------------------
divline
e_header "Disabling Gatekeeper..."
sudo spctl --master-disable

# Before relying on Homebrew, check that packages can be compiled
if ! type_exists 'gcc'; then

    e_header "Installing MacOSX Command Line Tools..."
    xcode-select --install

else

    e_already_installed "XCode Command Line Tools"

fi
