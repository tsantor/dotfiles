#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# File System
# -----------------------------------------------------------------------------
# Delete a file or folder
alias del='rm --preserve-root'

# Delete all files and folders in current directory
deleteall() {
    if confirm "Delete all files"; then
        rm -rf *
    fi
}

# Make a new file
alias mkfile="touch"

# Create directory and any parent directories needed
alias mkdir="mkdir -pv"

# Create directory and enter it
mkd() {
    mkdir "$@" && cd "$@"
}

# Backup a file
backup () {
    cp $1 ${1}-`date +%Y%m%d%H%M`.backup
}

# Backup a file to the current directory and log it
bu() {
    cp "$@" "$@".backup-`date +%y%m%d`
    echo "`date +%Y-%m-%d` backed up $PWD/$@" >> ~/backups.log
}

# Symlink a file or directory
alias symlink="ln -sfv"

alias diff='colordiff'

# -----------------------------------------------------------------------------
# Archive Handling
# -----------------------------------------------------------------------------
# Extract archive
# Example: extract tarball.tar
extract() {
    if [ -z "$1" ]; then
        # display usage if no parameters given
        echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    else
        if [ -f $1 ] ; then
            # NAME=${1%.*}
            # mkdir $NAME && cd $NAME
            case $1 in
              *.tar.bz2)   tar xvjf ../$1    ;;
              *.tar.gz)    tar xvzf ../$1    ;;
              *.tar.xz)    tar xvJf ../$1    ;;
              *.lzma)      unlzma ../$1      ;;
              *.bz2)       bunzip2 ../$1     ;;
              *.rar)       unrar x -ad ../$1 ;;
              *.gz)        gunzip ../$1      ;;
              *.tar)       tar xvf ../$1     ;;
              *.tbz2)      tar xvjf ../$1    ;;
              *.tgz)       tar xvzf ../$1    ;;
              *.zip)       unzip ../$1       ;;
              *.Z)         uncompress ../$1  ;;
              *.7z)        7z x ../$1        ;;
              *.xz)        unxz ../$1        ;;
              *.exe)       cabextract ../$1  ;;
              *)           e_error "extract: '$1' - unknown archive method" ;;
            esac
        else
            e_error "$1 - file does not exist"
        fi
    fi
}

# Create an archive (*.tar.gz) of a file or folder
# Example: maketar dirname
maketar() {
    tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"
}

# Create an archive (*.zip) of a file or folder
# Example: makezip dirname
makezip() {
    zip -r "${1%%/}.zip" "$1"
}

# -----------------------------------------------------------------------------
# Drive, Directory and File Info
# -----------------------------------------------------------------------------
alias driveinfo="df -h"
alias filesize="du -sh ${1}"

# -----------------------------------------------------------------------------
# Search
# -----------------------------------------------------------------------------
findfile() {
    if [ "${OS}" = "OSX" ]; then

        locate ${1} | grep ${1}

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        locate ${1} | grep ${1}

    elif [ "${OS}" = "Fedora" ]; then

        mlocate ${1} | grep ${1}

    elif [ "${OS}" = "CentOS" ]; then

        locate ${1} | grep ${1}

    fi

}

# Quickly search for file (eg - qfind *.md)
alias qfind="find . -name "

# Find an exectuable (eg - search git)
alias search="type -a"

# -----------------------------------------------------------------------------
