# -----------------------------------------------------------------------------
# Install Community Approved Repositories
# -----------------------------------------------------------------------------
divline
e_header "Installing Community Approved Repositories..."

# Community Approved Repositories
# Install Epel
if is_package_not_installed "epel-release"; then
    e_header "Installing Epel Repository..."

    if [ "${OS}" = "Fedora" ]; then

        e_warning "Skipping epel-release on Fedora..."

    elif [ "${OS}" = "CentOS" ]; then

        sudo rpm --import http://download.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
        sudo rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm &> /dev/null

    fi

    e_success "DONE!"
fi

# Install IUS
if is_package_not_installed "ius-release"; then
    e_header "Installing IUS Repository..."

    if [ "${OS}" = "Fedora" ]; then

        e_warning "Skipping uis-release on Fedora..."

    elif [ "${OS}" = "CentOS" ]; then

        sudo rpm --import http://dl.iuscommunity.org/pub/ius/IUS-COMMUNITY-GPG-KEY
        sudo rpm -Uvh http://dl.iuscommunity.org/pub/ius/stable/CentOS/6/x86_64/ius-release-1.0-13.ius.centos6.noarch.rpm &> /dev/null
    fi

    e_success "DONE!"
fi

: <<'END'
# Install ELRepo
if is_package_not_installed "elrepo-release"; then
    e_header "Installing ELRepo Repository..."

    if [ "${OS}" = "Fedora" ]; then

        e_warning "Skipping elrepo-release on Fedora..."

    elif [ "${OS}" = "CentOS" ]; then

        sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org &> /dev/null
        sudo rpm -Uvh http://www.elrepo.org/elrepo-release-6-6.el6.elrepo.noarch.rpm &> /dev/null

    fi

    e_success "DONE!"
fi
END

# -----------------------------------------------------------------------------
# Other Third Party Repositories
# -----------------------------------------------------------------------------
divline
e_header "Installing Other Third Party Repositories..."

: <<'END'
# Install RPMFusion
if is_package_not_installed "rpmfusion-free-release"; then
    e_header "Installing RPMFusion Repository..."

    if [ "${OS}" = "Fedora" ]; then

        sudo yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
        sudo yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

    elif [ "${OS}" = "CentOS" ]; then

        sudo yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/free/el/updates/6/x86_64/rpmfusion-free-release-6-1.noarch.rpm
        sudo yum localinstall -y --nogpgcheck http://download1.rpmfusion.org/nonfree/el/updates/6/i386/rpmfusion-nonfree-release-6-1.noarch.rpm

    fi

    e_success "DONE!"
fi
END


: <<'END'
# Install Remi (Known Problem Repositoriy - http://wiki.centos.org/AdditionalResources/Repositories/)
if is_package_not_installed "remi-release"; then
    e_header "Installing Remi Repository..."

    if [ "${OS}" = "Fedora" ]; then

        wget http://rpms.famillecollet.com/remi-release-20.rpm &> /dev/null
        sudo yum install -y remi-release-20.rpm
        sudo rm remi-release-20.rpm

    elif [ "${OS}" = "CentOS" ]; then

        sudo rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm &> /dev/null

    fi

    sudo sed -ir '0,/^enabled\=\(.*\)$/s/^enabled\=\(.*\)$/enabled\=1/' /etc/yum.repos.d/remi.repo
    e_note "Enabled the [remi] repo in /etc/yum.repos.d/remi.repo"

    e_success "DONE!"
fi
END

# Install ATrpms
if is_package_not_installed "atrpms-repo"; then
    e_header "Installing ATrpms Repository..."

    if [ "${OS}" = "Fedora" ]; then

        sudo rpm -Uvh http://dl.atrpms.net/all/atrpms-repo-20-7.fc20.x86_64.rpm &> /dev/null

        # Disable repo (we will only use it when specified)
        sudo sed -ir '0,/^enabled\=\(.*\)$/s/^enabled\=\(.*\)$/enabled\=0/' /etc/yum.repos.d/atrpms.repo
        e_note "Disabled the [atrpms] repo in /etc/yum.repos.d/atrpms.repo. Use manually via '--enablerepo=atrpms'"
        e_command "Example: sudo yum install -y --enablerepo=atrpms ffmpeg"

    elif [ "${OS}" = "CentOS" ]; then

        sudo rpm --import http://packages.atrpms.net/RPM-GPG-KEY.atrpms
        sudo rpm -Uvh http://dl.atrpms.net/el6-x86_64/atrpms/stable/atrpms-repo-6-7.el6.x86_64.rpm &> /dev/null

        # Disable repo (we will only use it when specified)
        sudo sed -ir '0,/^enabled\=\(.*\)$/s/^enabled\=\(.*\)$/enabled\=0/' /etc/yum.repos.d/atrpms.repo
        e_note "Disabled the [atrpms] repo in /etc/yum.repos.d/atrpms.repo. Use manually via '--enablerepo=atrpms'"
        e_command "Example: sudo yum install -y --enablerepo=atrpms ffmpeg"

    fi

    e_success "DONE!"
fi
