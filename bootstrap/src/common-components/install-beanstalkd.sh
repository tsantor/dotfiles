# -----------------------------------------------------------------------------
# Install Beanstalkd
# -----------------------------------------------------------------------------
divline
e_header "Installing Beanstalkd..."

if confirm "Install Beanstalkd"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "beanstalk"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "beanstalkd"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "beanstalkd"

    fi

    # Start service
    # execute "beanstalkd_start" "Starting Beanstalkd..."

    # Start on boot
    # execute "beanstalkd_start_on_boot" "Start Beanstalkd on boot"

fi
