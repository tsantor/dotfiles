#!/usr/bin/env bash

alias list_daemons="ls /Library/LaunchDaemons"
alias list_agents="ls ~/Library/LaunchAgents/"

# Ring the terminal bell, and put a badge on Terminal.app’s Dock icon
# (useful when executing time-consuming commands)
alias badge="tput bel"

# -----------------------------------------------------------------------------
# MISC.
# -----------------------------------------------------------------------------
# Merge PDF files
# Usage: `mergepdf -o output.pdf input{1,2,3}.pdf`
alias mergepdf='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py'

# VLC
alias vlc='/Applications/VLC.app/Contents/MacOS/VLC'

# Export markdown file to HTML
alias md2html='grip --export '

# Moves a file to the MacOS trash
trash() {
    command mv "$@" ~/.Trash
}

# -----------------------------------------------------------------------------
# House Cleaning
# -----------------------------------------------------------------------------
# Recursively delete .DS_Store files
alias cleanup_dsstore="find . -type f -name '*.DS_Store' -ls -delete"

# Clean up LaunchServices to remove duplicates in the "Open With" menu
alias cleanup_launchservices="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

# Empty the Trash on all mounted volumes and the main HDD
# Also, clear Apple’s System Logs to improve shell startup speed
alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes/*; sudo rm -rfv ~/.Trash/*"

# Securely erase free space (1-single pass, 2-7 pass, 3-35 pass)
alias secureerase="diskutil secureErase freespace 2 /dev/disk0"

# -----------------------------------------------------------------------------
# Toggle Features
# -----------------------------------------------------------------------------
# Disable/Enable Spotlight
alias spotoff="sudo mdutil -a -i off"
alias spoton="sudo mdutil -a -i on"

# Disable/Enable Notification center
# alias notificationcenterenable="launchctl load -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist"
# alias notificationcenterdisable="launchctl unload -w /System/Library/LaunchAgents/com.apple.notificationcenterui.plist"

# Disable/Enable hidden files in Finder
alias showhidden="defaults write com.apple.finder AppleShowAllFiles TRUE; killall Finder"
alias hidehidden="defaults write com.apple.finder AppleShowAllFiles FALSE; killall Finder"

# Hide/Show the desktop icons
# alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool FALSE; killall Finder"
# alias showdesktop="defaults write com.apple.finder CreateDesktop -bool TRUE; killall Finder"

# -----------------------------------------------------------------------------

alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
alias firefox="/Applications/Firefox.app/Contents/MacOS/firefox"
