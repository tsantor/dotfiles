# -----------------------------------------------------------------------------
# Install MemCached
# -----------------------------------------------------------------------------
divline
e_header "Installing Redis..."

if confirm "Install Redis"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "redis"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "redis-server"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "redis"

    fi

    # Start service
    execute "redis_start" "Starting Redis..."

    # Start on boot
    execute "redis_start_on_boot" "Start Redis on boot"

fi
