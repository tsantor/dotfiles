# -----------------------------------------------------------------------------
# Install ffmpeg
# -----------------------------------------------------------------------------
divline
e_header "Installing ffmpeg..."

if confirm 'Install ffmpeg?'; then
    if is_package_not_installed "ffmpeg"; then

        e_installing "ffmpeg"
        brew install ffmpeg --with-libvorbis --with-speex --with-libvpx >> ${PWD}/bootstrap.log 2>&1

    fi
fi
