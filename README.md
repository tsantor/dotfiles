# Tim's dotfiles
Author: Tim Santor <tsantor@xstudios.com>

## Installation
Clone the repository to your `~` home directory.

```bash
cd ~
git clone https://bitbucket.org/tsantor/dotfiles.git dotfiles
```

> Note: Installation may overwrite existing dotfiles in your `~` home directory. Be sure you backup any files you wish to preserve.

The bootstrapper script will pull in the latest version and symlink the files to your home directory.

To symlink all files to your `~/dotfiles` directory:

```bash
cd ~/dotfiles/bin
./symlink-dotfiles
```

## How to update

To bootstrap again at any time just run `bs` in the terminal.  This will update your `~/dotfiles` repo, and then automatically bootstrap your system again all in one simple command.

> Note: Any dotfiles you have created that _are not_ managed by this repo __will not__ be overwritten.

## System Bootstrap

- **OSX** - bootstrap/osx-bootstrap.sh
- **Debian/Ubuntu** - bootstrap/debian-bootstrap.sh
- **Fedora/RedHat/CentOS** - bootstrap/fedora-bootstrap.sh


## Acknowledgements

Inspiration and code was taken from many sources, including:

- @necolas (Nicolas Gallagher) https://github.com/necolas/dotfiles
- @mathiasbynens (Mathias Bynens) https://github.com/mathiasbynens/dotfiles
- @tejr (Tom Ryder) https://github.com/tejr/dotfiles
- @gf3 (Gianni Chiappetta) https://github.com/gf3/dotfiles
- @cowboy (Ben Alman) https://github.com/cowboy/dotfiles
- @alrra (Cãtãlin Mariş) https://github.com/alrra/dotfiles
- @phlco (Phillip Lamplugh) https://github.com/phlco/installfest_scripts
