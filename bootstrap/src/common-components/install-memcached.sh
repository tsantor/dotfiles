# -----------------------------------------------------------------------------
# Install MemCached
# -----------------------------------------------------------------------------
divline
e_header "Installing MemCached..."

if confirm "Install MemCached"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "memcached"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "memcached"
        package_install "libmemcached-tools"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "memcached"
        package_install "libmemcached"

    fi

    # Start service
    execute "memcached_start" "Starting MemCached..."

    # Start on boot
    execute "memcached_start_on_boot" "Start MemCached on boot"

fi
