# -----------------------------------------------------------------------------
# Install Nginx
# -----------------------------------------------------------------------------
divline
e_header "Installing Nginx..."

if confirm "Install Nginx"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "nginx"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        package_install "nginx"

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "nginx"

    fi

    # Start service
    execute "nginx_start" "Starting Nginx..."

    # Start on boot
    execute "nginx_start_on_boot" "Start Nginx on boot"

fi
