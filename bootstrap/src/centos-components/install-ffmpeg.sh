# -----------------------------------------------------------------------------
# Install ffmpeg
# -----------------------------------------------------------------------------
divline
e_header "Installing ffmpeg..."

if confirm "Install ffmpeg"; then
    if is_package_not_installed "ffmpeg"; then

        if [ "${OS}" = "Fedora" ]; then

            package_install "ffmpeg"

        elif [ "${OS}" = "CentOS" ]; then

            sudo yum install -y --enablerepo=atrpms ffmpeg >> ${PWD}/bootstrap.log 2>&1 # Using the atrpms repo

        fi

    fi
fi
