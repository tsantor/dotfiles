#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Laravel
# -----------------------------------------------------------------------------
# alias laravel_clearlog="echo '' > app/storage/logs/laravel.log"
# alias laravel_viewlog="cat app/storage/logs/laravel.log"

# # Artisan
# alias artisan="php artisan "
# alias artisan_serve="php artisan serve --host=127.0.0.1 --port=9000"
# alias artisan_queuelisten="php artisan queue:listen --tries=3 --sleep=5"
# alias artisan_queueretry="php artisan queue:retry"
# alias artisan_queueflush="php artisan queue:flush"

# # Generator Stuff
# alias artisan_genmodel="php artisan generate:model"
# alias artisan_gencontroller="php artisan generate:controller"
# alias artisan_genview="php artisan generate:view"
# alias artisan_genseed="php artisan generate:seed"
# alias artisan_genmigration="php artisan generate:migration"
# alias artisan_genresource="php artisan generate:resource"

# # Composer
# alias composer_clearcache="rm -rf ~/.composer/cache"

# -----------------------------------------------------------------------------

