# -----------------------------------------------------------------------------
# Config PHP
# -----------------------------------------------------------------------------
divline
e_header "Setting sensible PHP defaults..."

sed_phpini_replace() {

    # Set display_errors
    #sed -ir 's/^display_errors \= \([a-zA-Z]*\)$/display_errors \= On/' $1 >/dev/null 2>&1

    # Set error_reporting
    sudo sed -ir 's/^error_reporting \= \(.*\)$/error_reporting \= E_ALL | E_STRICT/' $1 &> /dev/null

    # Set date.timezone (default none)
    sudo sed -ir 's/^;date\.timezone \= \(.*\)$/date.timezone \= UTC/' $1 &> /dev/null

    # Set post_max_size (default 2M)
    sudo sed -ir 's/^post_max_size \= \(.*\)$/post_max_size \= 500M/' $1 &> /dev/null

    # Set upload_max_filesize (default 16M)
    sudo sed -ir 's/^upload_max_filesize \= \(.*\)$/upload_max_filesize \= 500M/' $1 &> /dev/null

    # Set max_execution_time (default 30)
    sudo sed -ir 's/^max_execution_time \= \(.*\)$/max_execution_time \= 300/' $1 &> /dev/null

    # Set max_input_time (default 60)
    sudo sed -ir 's/^max_input_time \= \(.*\)$/max_input_time \= 300/' $1 &> /dev/null

    # Set memory_limit (default 128M)
    sudo sed -ir 's/^memory_limit \= \(.*\)$/memory_limit \= 128M/' $1 &> /dev/null

}

set_php_defaults() {

    #e_note "Red Hat/CentOS: /etc/php.ini"
    #e_note "Debian/Ubuntu: /etc/php5/apache2/php.ini"
    #e_note "Mac OSX: /usr/local/etc/php/*/php.ini"
    #echo -n "Enter the full path to your php.ini (eg - /usr/local/etc/php/*/php.ini, /etc/php.ini, etc.): "
    #read path

    # Auto determine the phi.ini path
    if [ "${OS}" = "OSX" ]; then
        path=/usr/local/etc/php/*/php.ini
    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then
        path=/etc/php/7.0/cli/php.ini
    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then
        path=/etc/php.ini
    fi

    #local path=$(get_phpini_location)

    if file_exists "${path}"; then

        # Backup file before doing anything
        execute "sudo cp ${path} ${path}.bak" "Backed up existing php.ini to ${path}.bak"

        execute "sed_phpini_replace ${path}" "Sensible PHP defaults"

        # Restart apache
        execute "apache_restart" "Restarting Apache..."
        e_warning "Remember to restart Apache and/or Nginx"
    else
        e_warning "${path} does not exist"
    fi

}

if confirm "Set sensible PHP defaults"; then
    set_php_defaults
fi
