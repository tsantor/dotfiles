# -----------------------------------------------------------------------------
# Install ffmpeg
# -----------------------------------------------------------------------------
divline
e_header "Installing ffmpeg..."
# https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu

if confirm "Install ffmpeg"; then
    if is_not_installed "ffmpeg"; then

        # Install dependencies
        e_header "Installing dependencies..."
        package_install "autoconf"
        package_install "automake"
        package_install "build-essential"
        package_install "libass-dev"
        package_install "libfreetype6-dev"
        package_install "libsdl2-dev"
        package_install "libtheora-dev"
        package_install "libtool"
        package_install "libva-dev"
        package_install "libvdpau-dev"
        package_install "libvorbis-dev"
        package_install "libxcb1-dev"
        package_install "libxcb-shm0-dev"
        package_install "libxcb-xfixes0-dev"
        package_install "pkg-config"
        package_install "texinfo"
        package_install "wget"
        package_install "zlib1g-dev"

        package_install "yasm"
        package_install "libav-tools"
        package_install "libx264-dev"
        package_install "libx265-dev"
        package_install "libfdk-aac-dev"
        package_install "libmp3lame-dev"
        package_install "libopus-dev"
        package_install "libvpx-dev"

        mkdir ~/ffmpeg_sources && cd ~/ffmpeg_sources

        e_note "Downloading ffmpeg..."
        wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2

        e_note "Untarring ffmpeg-*.tar.bz2..."
        tar xjvf ffmpeg-snapshot.tar.bz2

        e_header "Configuring and making ffmpeg install. This may take a while..."
        cd ffmpeg/

        ./configure \
        --prefix="$HOME/ffmpeg_build" \
        --pkg-config-flags="--static" \
        --extra-cflags="-I$HOME/ffmpeg_build/include" \
        --extra-ldflags="-L$HOME/ffmpeg_build/lib" \
        --enable-gpl \
        --enable-libass \
        --enable-libfdk-aac \
        --enable-libfreetype \
        --enable-libmp3lame \
        --enable-libopus \
        --enable-libtheora \
        --enable-libvorbis \
        --enable-libvpx \
        --enable-libx264 \
        --enable-libx265 \
        --enable-nonfree
        # make #&> /dev/null

        sudo make install #&> /dev/null

        cd ~
        execute "sudo rm -rf ffmpeg" "Removed ffmpeg source files"
    fi
fi
