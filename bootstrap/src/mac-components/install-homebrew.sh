# -----------------------------------------------------------------------------
# Homebrew
# -----------------------------------------------------------------------------
if test ! $(which brew); then
    divline
    echo "=> Installing Homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi
