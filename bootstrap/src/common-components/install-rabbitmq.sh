# -----------------------------------------------------------------------------
# Install RabbitMQ
# -----------------------------------------------------------------------------
divline
e_header "Installing RabbitMQ..."

if confirm "Install RabbitMQ"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "rabbitmq"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        #package_install "rabbitmq-server" # provides outdated package

        if is_package_not_installed "rabbitmq-server"; then
            # Add the following line to /etc/apt/sources.list:
            sudo sed -i "\$adeb http://www.rabbitmq.com/debian/ testing main" /etc/apt/sources.list

            # To avoid warnings about unsigned packages, add public key
            wget http://www.rabbitmq.com/rabbitmq-signing-key-public.asc #&> /dev/null
            sudo apt-key add rabbitmq-signing-key-public.asc #&> /dev/null
            sudo rm rabbitmq-signing-key-public.asc

            sudo apt-get update
            package_install "rabbitmq-server"
        fi

    elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

        package_install "erlang"
        #package_install "rabbitmq-server" # provides outdated package

        if is_package_not_installed "rabbitmq-server"; then
            # Manually get and install latest version
            e_installing "rabbitmq-server"
            wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.4.0/rabbitmq-server-3.4.0-1.noarch.rpm
            sudo rpm --import http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
            sudo yum install -y rabbitmq-server-3.4.0-1.noarch.rpm >> ${PWD}/bootstrap.log 2>&1;
            sudo rm rabbitmq-server-3.4.0-1.noarch.rpm
            e_install_success "rabbitmq-server"
        fi

    fi

    if confirm "Start RabbitMQ on boot"; then
        # Start service
        execute "rabbitmq_start" "Starting RabbitMQ..."

        # Start on boot
        execute "rabbitmq_start_on_boot" "Start RabbitMQ on boot"
    fi

fi
