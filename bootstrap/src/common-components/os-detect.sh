# -----------------------------------------------------------------------------
# Detect OS
# -----------------------------------------------------------------------------
OS=$(get_os)

# Ask the user if our assumption is correct (user can manually override if not)
if ! confirm "The current OS is ${OS}"; then
    echo -n "Enter the correct OS type (eg - OSX, Ubuntu or CentOS): "
    read os_type
    OS=$os_type
fi
