#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Network
# -----------------------------------------------------------------------------

# App with connections
alias openconnections="lsof -i "
alias connectedapps="lsof -i | grep ESTABLISHED"

# Enhanced WHOIS lookups
alias whois="whois -h whois-servers.net"

# Port scan
alias sshscan="nmap -p 22 localhost"
alias netscan="nmap -F localhost"
alias openports="sudo lsof -i -P | grep -i \"listen\""

killallonport() {
    lsof -i tcp:${1} | awk 'NR!=1 {print $2}' | xargs kill
}
alias killallonport=killallonport

# -----------------------------------------------------------------------------
