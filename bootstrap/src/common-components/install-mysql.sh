# -----------------------------------------------------------------------------
# Install MySQL
# -----------------------------------------------------------------------------
divline
e_header "Installing MySQL..."

if confirm "Install MySQL"; then

    if [ "${OS}" = "OSX" ]; then

        package_install "mysql"

    elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

        # Input desired default root password when prompted
        sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password newmedia'
        sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password newmedia'

        package_install "mysql-server"

    elif [ "${OS}" = "Fedora" ]; then

        package_install "mariadb-galera-server"

    elif [ "${OS}" = "CentOS" ]; then

        package_install "mariadb-server"

    fi

    # Start service
    execute "mysql_start" "Starting MySQL..."

    e_header "Securing MySQL installation..."
    sudo mysql_secure_installation

    # Start on boot
    execute "mysql_start_on_boot" "Start MySQL on boot"

fi
