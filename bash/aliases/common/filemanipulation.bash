#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Text file manipulation
# -----------------------------------------------------------------------------
# Strip lines from text file
# Example: strip_lines_with "404 Not Found" ./laravel.log
strip_lines_with() {
    sed -i "/${1}/d" ${2}
}

tidy_xml() {
    tidy -quiet -asxml -xml -indent -wrap 1024 --hide-comments 1 ${1} >> tidy_output.xml
}
