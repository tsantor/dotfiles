#!/usr/bin/env bash

# alias php5-fpm.start="launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.php56.plist"
# alias php5-fpm.stop="launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.php56.plist"
# alias php5-fpm.restart='php5-fpm.stop && php5-fpm.start'

# alias php7-fpm.start="launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.php70.plist"
# alias php7-fpm.stop="launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.php70.plist"
# alias php7-fpm.restart='php7-fpm.stop && php7-fpm.start'

# lsof -Pni4 | grep LISTEN | grep php
# php -i | grep ini
# locate php-fpm.conf
# listen = /var/run/php5-fpm.sock
# syslog -w

# alias nginx.logs.error='tail -250f /usr/local/etc/nginx/logs/error.log'
# alias nginx.logs.access='tail -250f /usr/local/etc/nginx/logs/access.log'
# alias nginx.logs.default.access='tail -250f /usr/local/etc/nginx/logs/default.access.log'
# alias nginx.logs.default-ssl.access='tail -250f /usr/local/etc/nginx/logs/default-ssl.access.log'
# alias nginx.logs.phpmyadmin.error='tail -250f /usr/local/etc/nginx/logs/phpmyadmin.error.log'
# alias nginx.logs.phpmyadmin.access='tail -250f /usr/local/etc/nginx/logs/phpmyadmin.access.log'

alias pdfconcat="/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py"

alias finderscope="firefox \
    -new-tab -url https://bitbucket.org/finderscope \
    -new-tab -url https://drive.google.com/drive/u/1/my-drive \
    -new-tab -url https://console.cloud.google.com \
    -new-tab -url https://sentry.io/organizations/finderscope/issues/ \
    -new-tab -url https://finderscope.monday.com/ \
    -new-tab -url https://finderscope.atlassian.net/jira/software/c/projects/FA/issues/ \
    -new-tab -url https://cloud.digitalocean.com/projects/70ebd871-c1b8-4be1-8cfe-2a3cc79d6f65/resources?i=c49b78"
