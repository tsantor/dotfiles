# -----------------------------------------------------------------------------
# Script Header
# -----------------------------------------------------------------------------
if [ "${OS}" = "OSX" ]; then

    script_header "Mac OSX Bootstrap"

elif [ "${OS}" = "Debian" ] || [ "${OS}" = "Ubuntu" ]; then

    script_header "Debian / Ubuntu Bootstrap"

elif [ "${OS}" = "Fedora" ] || [ "${OS}" = "CentOS" ]; then

    script_header "Fedora / Red Hat / CentOS Bootstrap"

fi
